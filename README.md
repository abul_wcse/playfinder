# PlayFinder My Local Pitch
A small RESTful API with the following endpoints:
 - GET /pitches
 - GET /pitches/<id>
 - GET /pitches/<id>/slots
 - POST /pitches/<id>/slots

## How to run the application locally

To run the application locally, Follow the below steps 

- Clone the repository
- Update the DATABASE_URL in .env file (currently supports mysql)
- Run `php composer.phar install`
- To create the database, run the following command, `./bin/console doctrine:database:create`
- To create the database schema, run the following command `./bin/console doctrine:migrations:migrate`
- Now, finally run `symfony serve` and visit local url. The index page should show the API docs

## How to seed data into the database

Run the following command to seed the database with dummy data.



## How to run unit test

Run the following command to execute the unit test

' ./bin/console --env=dev doctrine:fixtures:load '

## How to run Functional test

Run the following command to execute the functional test

`./bin/phpunit --testsuite="FunctionalTest"`

## How to create postman collection testsuite

- Open Postman
- File -> Import -> Link
- The link will be {site_url}/api/doc.json
- Click import
- This will create a new collection for you, update the example wherever need ans update the baseurl variable.
- You are good to go.  
