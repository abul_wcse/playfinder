<?php
namespace PlayFinder\EventListener;

use PlayFinder\Exception\PlayFinderException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        if (!$exception instanceof PlayFinderException) {
            return;
        }

        $event->setResponse(new JsonResponse([
            'error' => $exception->getMessage()
        ], $exception->getCode(), [
                'Content-Type' => 'application/problem+json'
            ]
        ));
        $event->stopPropagation();
    }
}
