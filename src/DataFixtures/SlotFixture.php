<?php

namespace PlayFinder\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use PlayFinder\Factory\CurrencyFactory;
use PlayFinder\Factory\PitchFactory;
use PlayFinder\Factory\SlotFactory;

class SlotFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        SlotFactory::createMany(20, [
            'pitch' => PitchFactory::random(),
            'currency' => CurrencyFactory::random()
        ]);
    }

    private function getRandomSlotDetail()
    {

    }
}
