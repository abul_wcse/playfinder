<?php

namespace PlayFinder\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use PlayFinder\Factory\PitchFactory;

class PitchFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        PitchFactory::createMany(10);
    }

}
