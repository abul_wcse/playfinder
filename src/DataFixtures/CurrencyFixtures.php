<?php

namespace PlayFinder\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use PlayFinder\Factory\CurrencyFactory;

class CurrencyFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        CurrencyFactory::createMany(10);
    }
}
