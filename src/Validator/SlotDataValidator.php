<?php


namespace PlayFinder\Validator;

use PlayFinder\Exception\InvalidDataException;
use PlayFinder\Repository\CurrencyRepository;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

class SlotDataValidator
{

    /**
     * @var CurrencyRepository
     */
    private CurrencyRepository $currencyRepository;

    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @param $data
     *
     * @return true
     * @throws InvalidDataException
     */
    public function validate($data)
    {
        if (!is_array($data)) {
            throw new InvalidDataException("Type Error: data should have array of slots.");
        }

        $validator = Validation::createValidator();

        foreach ($data as $datum) {
            $violationList = $validator->validate($datum, $this->getValidationConstraint());
            if ($violationList->count() > 0) {
                throw new InvalidDataException(sprintf("invalid details given: %s", (string) $violationList));
            }
            $currency = $datum['attributes']['currency'] ?? null;
            if ($this->currencyRepository->findOneBy(['name' => $currency]) === null ) {
                throw new InvalidDataException(sprintf("Currency %s is not supported", $currency));
            }
        }

        return true;
    }

    /**
     * @return Assert\Collection
     */
    private function getValidationConstraint()
    {
        return new Assert\Collection([
            'type' => new Assert\EqualTo('slots'),
            'id' => new Assert\IsNull(),
            'attributes' => new Assert\Collection([
                    'starts' => new DateTimeString(),
                'ends' => new DateTimeString(),
                'price' => new Assert\GreaterThanOrEqual(0),
                'currency' => new Assert\Required(),
                'available' => new Assert\Type('bool')
            ])
        ]);
    }
}
