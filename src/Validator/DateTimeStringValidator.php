<?php

namespace PlayFinder\Validator;

use DateTime;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DateTimeStringValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \PlayFinder\Validator\DateTimeString */

        if (null === $value || '' === $value) {
            return;
        }

        if (DateTime::createFromFormat('U', strtotime($value)) === false) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value)
                ->addViolation();
        }

    }
}
