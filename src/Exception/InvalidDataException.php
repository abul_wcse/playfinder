<?php


namespace PlayFinder\Exception;


use Exception;
use Throwable;

class InvalidDataException extends Exception implements PlayFinderException
{

    public function __construct($message = "", Throwable $previous = null)
    {
        parent::__construct($message, 400, $previous);
    }
}
