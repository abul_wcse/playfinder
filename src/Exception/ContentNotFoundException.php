<?php


namespace PlayFinder\Exception;


use Exception;
use Throwable;

class ContentNotFoundException extends Exception implements PlayFinderException
{
    public function __construct($message = "", Throwable $previous = null)
    {
        parent::__construct($message, 404, $previous);
    }
}
