<?php

namespace PlayFinder\Controller;

use PlayFinder\Exception\ContentNotFoundException;
use PlayFinder\Exception\InvalidDataException;
use PlayFinder\Repository\PitchRepository;
use PlayFinder\Repository\SlotRepository;
use PlayFinder\Service\ConvertUserSlotDataToObject;
use PlayFinder\Validator\SlotDataValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

class PitchManageController
{
    /**
     * @var PitchRepository
     */
    private PitchRepository $repository;
    /**
     * @var SlotDataValidator
     */
    private SlotDataValidator $slotDataValidator;
    /**
     * @var ConvertUserSlotDataToObject
     */
    private ConvertUserSlotDataToObject $dataToObject;
    /**
     * @var SlotRepository
     */
    private SlotRepository $slotRepository;

    /**
     * PitchManageController constructor.
     * @param PitchRepository $repository
     */
    public function __construct(PitchRepository $repository, SlotRepository $slotRepository, SlotDataValidator $slotDataValidator, ConvertUserSlotDataToObject $dataToObject)
    {
        $this->repository = $repository;
        $this->slotDataValidator = $slotDataValidator;
        $this->dataToObject = $dataToObject;
        $this->slotRepository = $slotRepository;
    }

    #[Route('/pitches', name: 'pitch_index', methods: ['GET'])]

    /**
     * @OA\Get (
     *     description="Get list of all pitches",
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns the list of all pitches",
     *     @OA\JsonContent(
     *      type="array",
     *        @OA\Items(
     *           @OA\Property(property="mete",type="object",
     *              @OA\Property(property="total_items:", type="integer")
    *           ),
     *          @OA\Property(property="data",type="array",
     *              @OA\Items(
     *                  @OA\Property(property="type", type="string"),
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="attributes", type="object",
     *                      @OA\Property(property="name", type="string"),
     *                      @OA\Property(property="sport", type="string")
     *                  ),
     *              )
     *           )
     *       )
     *     )
     * )
     */
    public function index(): Response
    {
        $pitches = $this->repository->findAll();
        return new JsonResponse([
            'meta' => [
                'total_items' => count($pitches)
            ],
            'data' => $pitches
        ]);
    }

    #[Route('/pitches/{id}', name: 'pitch_detail', methods: ['GET'])]
    /**
     *  @OA\Get (
     *     description="Get details about a specific pitch by id",
     * )
     * @OA\Parameter (
     *      name="id",
     *      description="Unique identifier of the pitch",
     *     in="path"
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns the list of all pitches",
     *     @OA\JsonContent(
     *          type="object",
     *          @OA\Property(property="data",type="object",
     *              @OA\Property(property="type", type="string"),
     *              @OA\Property(property="id", type="integer"),
     *              @OA\Property(property="attributes", type="object",
     *                  @OA\Property(property="name", type="string"),
     *                  @OA\Property(property="sport", type="string")
     *              ),
     *          )
     *      )
     *  )
     */
    public function show($id): Response
    {
        $pitch = $this->repository->find($id);

        if ($pitch === null) {
            throw new ContentNotFoundException(sprintf('Unable to find the pitch with id %s', $id));
        }
        return new JsonResponse(['data' => $pitch]);
    }

    #[Route('/pitches/{id}/slots', name: 'pitch_slot_list', methods: ['GET'])]
    /**
     * @OA\Get (
     *     description="Get list of all slots associated with the pitch",
     * )
     * @OA\Parameter (
     *      name="id",
     *      description="Unique identifier of the pitch",
     *     in="path"
     * )
     *
     *  @OA\Response(
     *     response=200,
     *     description="Returns the list of all pitches",
     *     @OA\JsonContent(
     *      type="array",
     *        @OA\Items(
     *           @OA\Property(property="mete",type="object",
     *              @OA\Property(property="total_items:", type="integer")
     *           ),
     *          @OA\Property(property="data",type="array",
     *              @OA\Items(
     *                  @OA\Property(property="type", type="string"),
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="attributes", type="object",
     *                      @OA\Property(property="starts", type="string", format="datetime", example="2016-11-04T10:00:00+00:00"),
     *                      @OA\Property(property="ends", type="string", format="datetime", example="2016-11-04T10:00:00+00:00"),
     *                      @OA\Property(property="price", type="string"),
     *                      @OA\Property(property="currency", type="string"),
     *                      @OA\Property(property="available:", type="boolean"),
     *                  ),
     *              )
     *           )
     *       )
     *     )
     * )

     */
    public function showSlots($id): Response
    {
        $pitch = $this->repository->find($id);
        if ($pitch === null) {
            throw new ContentNotFoundException(sprintf('Unable to find the pitch with id %s', $id));
        }
        $slots = $pitch->getSlots();
        return new JsonResponse([
            [
                'meta' => [
                    'total_items' => $slots->count()
                ]
            ],
            [
                'data' => $slots->toArray()
            ]
        ]);
    }

    #[Route('/pitches/{id}/slots', name: 'add_pitch_slot', methods: ['POST'])]
    /**
     * @OA\Post  (
     *     description="Get list of all slots associated with the pitch",
     * )
     * @OA\Parameter (
     *      name="id",
     *      description="Unique identifier of the pitch",
     *     in="path"
     * )
     *
     * @OA\RequestBody(
     *     @OA\JsonContent(
     *      type="array",
     *        @OA\Items(
     *          @OA\Property(property="data",type="array",
     *              @OA\Items(
     *                  @OA\Property(property="type", type="string", example="slot"),
     *                  @OA\Property(property="id", type="integer", format="null"),
     *                  @OA\Property(property="attributes", type="object",
     *                      @OA\Property(property="starts", type="string", format="datetime", example="2016-11-04T10:00:00+00:00"),
     *                      @OA\Property(property="ends", type="string", format="datetime", example="2016-11-04T10:00:00+00:00"),
     *                      @OA\Property(property="price", type="string"),
     *                      @OA\Property(property="currency", type="string"),
     *                      @OA\Property(property="available:", type="boolean"),
     *                  ),
     *              )
     *           )
     *       )
     *     )
     * )
     *
     * @OA\Response(
     *     response="201",
     *     description="Status of the add/update action"
     * )
     *
     */
    public function addNewSlots($id, Request $request): Response
    {
        $requestContent = $request->getContent();
        $pitch = $this->repository->find($id);
        if ($pitch === null) {
            throw new ContentNotFoundException(sprintf('Unable to find the pitch with id %s', $id));
        }

        $data = $data = json_decode($requestContent, true);
        if (empty($data) || !isset($data['data'])) {
            throw new InvalidDataException("data attribute is missing");
        }
        $slots = $data['data'];
        $this->slotDataValidator->validate($slots);
        $slots = $this->dataToObject->convert($slots, $pitch);
        foreach ($slots as $slot) {
            $this->slotRepository->persist($slot);
        }
        $this->slotRepository->flush();

        return new JsonResponse(true, 201);
    }
}
