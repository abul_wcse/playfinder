<?php


namespace PlayFinder\Service;


use DateTime;
use PlayFinder\Entity\Slot;
use PlayFinder\Repository\CurrencyRepository;
use PlayFinder\Repository\PitchRepository;
use PlayFinder\Repository\SlotRepository;

class ConvertUserSlotDataToObject
{
    /**
     * @var PitchRepository
     */
    private PitchRepository $pitchRepository;
    /**
     * @var CurrencyRepository
     */
    private CurrencyRepository $currencyRepository;
    /**
     * @var SlotRepository
     */
    private SlotRepository $slotRepository;

    public function __construct(PitchRepository $pitchRepository, CurrencyRepository $currencyRepository, SlotRepository $slotRepository)
    {
        $this->pitchRepository = $pitchRepository;
        $this->currencyRepository = $currencyRepository;
        $this->slotRepository = $slotRepository;
    }

    public function convert($data, $pitch)
    {
        $slots = [];
        foreach ($data as $datum) {
            if ((int)$datum['id'] > 0) {
                $slot = $this->slotRepository->find($datum['id']);
            }

            $slot = $slot ?? new Slot();
            $slot
                ->setStartTime(DateTime::createFromFormat('U', strtotime($datum['attributes']['starts'])))
                ->setEndTime(DateTime::createFromFormat('U', strtotime($datum['attributes']['ends'])))
                ->setPitch($this->pitchRepository->find($pitch))
                ->setCurrency($this->currencyRepository->findOneBy(['name' => $datum['attributes']['currency']]))
                ->setPrice((float)$datum['attributes']['price'])
                ->setAvailable((bool)$datum['attributes']['available']);

            $slots[] = $slot;
        }

        return $slots;
    }
}
