<?php

namespace PlayFinder\Tests\FunctionalTest\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PitchControllerTest extends WebTestCase
{
    public function testPitchesList(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/pitches');

        $this->assertResponseIsSuccessful();
    }

    public function testPitches(): void
    {
        $client = static::createClient();
        $client->setServerParameters([
            'HTTPS' => 'On',
            'HTTP_HOST' => '127.0.0.1:8000'
        ]);
        $crawler = $client->request('GET', '/pitches');

        $content = $client->getResponse()->getContent();
        $content = json_decode($content, true);

        $this->assertArrayHasKey('meta', $content);
        $this->assertArrayHasKey('data', $content);
        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseIsSuccessful();
    }

    public function testPitchDetails(): void
    {
        $client = static::createClient();
        $client->setServerParameters([
            'HTTPS' => 'On',
            'HTTP_HOST' => '127.0.0.1:8000'
        ]);

        $crawler = $client->request('GET', '/pitches');

        $content = $client->getResponse()->getContent();
        $content = json_decode($content, true);
        $pitches = $content['data'];

        if (count($pitches) > 0 ) {
            $pitch = array_pop($pitches);
            $client->request('GET', '/pitches/' . $pitch['id']);
            $content = $client->getResponse()->getContent();
            $this->assertJson($content);
            $content = json_decode($content, true);
            $this->assertArrayHasKey('data', $content);

        }
        $client->request('GET', '/pitches/-2');
        $this->assertJson($client->getResponse()->getContent());
        $this->assertResponseStatusCodeSame(404);
    }
}
