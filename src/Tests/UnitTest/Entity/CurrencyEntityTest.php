<?php

namespace PlayFinder\Tests\UnitTest\Entity;

use PHPUnit\Framework\TestCase;
use PlayFinder\Entity\Currency;

class CurrencyEntityTest extends TestCase
{
    public function testNameSetterAndGetter(): void
    {
        $value = 'Foo';
        $currency = new Currency();
        $currency->setName($value);
        self::assertEquals($value, $currency->getName());
    }

    public function testSymbolSetterAndGetter(): void
    {
        $value = '£';
        $currency = new Currency();
        $currency->setSymbol($value);
        self::assertEquals($value, $currency->getSymbol());
    }

    public function testJsonSerialise()
    {
        $currency = new Currency();
        $currency->setName('USD');
        $currency->setSymbol('$');

        self::assertEquals('{"type":"currency","id":"","attributes:":{"currency_code":"USD","currency_symbol":"$"}}', json_encode($currency));
    }
}
