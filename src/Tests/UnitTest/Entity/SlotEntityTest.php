<?php

namespace PlayFinder\Tests\UnitTest\Entity;

use DateTime;
use PHPUnit\Framework\TestCase;
use PlayFinder\Entity\Currency;
use PlayFinder\Entity\Slot;

class SlotEntityTest extends TestCase
{
    public function testStartTimeSetterAndGetter(): void
    {
        $value = new DateTime();
        $slot = new Slot();
        self::assertNull($slot->getStartTime());
        $slot->setStartTime($value);
        self::assertEquals($value, $slot->getStartTime());
    }

    public function testEndTimeSetterAndGetter(): void
    {
        $value = new DateTime();
        $slot = new Slot();
        self::assertNull($slot->getEndTime());
        $slot->setEndTime($value);
        self::assertEquals($value, $slot->getEndTime());
    }

    public function testPriceSetterAndGetter(): void
    {
        $value = 10.10;
        $slot = new Slot();
        self::assertNull($slot->getPrice());
        $slot->setPrice($value);
        self::assertEquals($value, $slot->getPrice());
    }

    public function testCurrencySetterAndGetter(): void
    {
        $value = new Currency();
        $slot = new Slot();
        self::assertNull($slot->getCurrency());
        $slot->setCurrency($value);
        self::assertEquals($value, $slot->getCurrency());
    }

    public function testAvaiableSetterAndGetter(): void
    {
        $value = true;
        $slot = new Slot();
        self::assertNull($slot->getAvailable());
        $slot->setAvailable($value);
        self::assertEquals($value, $slot->getAvailable());
    }

    public function testJsonSerialise()
    {
        $slot = new Slot();
        $slot->setAvailable(false);
        $slot->setPrice(10.50);
        $slot->setStartTime(new DateTime('01-01-2021'));
        $slot->setEndTime(new DateTime('02-01-2021'));

        $currency = new Currency();
        $currency->setName('GBP');
        $currency->setSymbol('£');

        $slot->setCurrency($currency);

        $expected = '{"type":"slots","id":"","attributes:":{"starts:":"2021-01-01T00:00:00+00:00","ends":"2021-01-02T00:00:00+00:00","price":10.5,"currency":"GBP","available":false}}';
        self::assertEquals($expected, json_encode($slot));
    }

}
