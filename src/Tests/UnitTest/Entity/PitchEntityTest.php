<?php

namespace PlayFinder\Tests\UnitTest\Entity;

use PHPUnit\Framework\TestCase;
use PlayFinder\Entity\Pitch;

class PitchEntityTest extends TestCase
{
    public function testNameSetterAndGetter(): void
    {
        $value = 'Foo';
        $pitch = new Pitch();
        $pitch->setName($value);
        self::assertEquals($value, $pitch->getName());
    }

    public function testSportSetterAndGetter(): void
    {
        $value = 'Cricket';
        $pitch = new Pitch();
        $pitch->setSport($value);
        self::assertEquals($value, $pitch->getSport());
    }

    public function testJsonSerialise()
    {
        $pitch = new Pitch();
        $pitch->setName('Foo');
        $pitch->setSport('Cricket');

        self::assertEquals('{"type":"pitches","id":"","attributes:":{"name":"Foo","sport":"Cricket"}}', json_encode($pitch));
    }
}
