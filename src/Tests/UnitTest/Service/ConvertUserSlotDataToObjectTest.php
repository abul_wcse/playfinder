<?php

namespace PlayFinder\Tests\UnitTest\Service;

use DateTime;
use PHPUnit\Framework\TestCase;
use PlayFinder\Entity\Currency;
use PlayFinder\Entity\Pitch;
use PlayFinder\Entity\Slot;
use PlayFinder\Repository\CurrencyRepository;
use PlayFinder\Repository\PitchRepository;
use PlayFinder\Repository\SlotRepository;
use PlayFinder\Service\ConvertUserSlotDataToObject;

class ConvertUserSlotDataToObjectTest extends TestCase
{
    public function testConvert(): void
    {
        $pitchRepository = self::getMockBuilder(PitchRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pitchRepository->expects($this->once())->method('find')->with(1)->willReturn(new Pitch());

        $slotRepository = self::getMockBuilder(SlotRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $slotRepository->expects($this->never())->method('find')->withAnyParameters();

        $currencyRepository = self::getMockBuilder(CurrencyRepository::class)
            ->disableOriginalConstructor()
            ->setMethodsExcept(['find'])
            ->getMock();
        $currencyRepository->expects($this->once())->method('findOneBy')->with(['name' => '$' ])->willReturn(new Currency());

        $converter = new ConvertUserSlotDataToObject($pitchRepository, $currencyRepository, $slotRepository);
        $slots = $converter->convert([
            [
                'type' => 'slots',
                'id' => null,
                'attributes' => [
                    'starts' => (new DateTime())->format('c'),
                    'ends' => (new DateTime())->format('c'),
                    'currency' => '$',
                    'price' => 10.51,
                    'available' => true,
                ]
            ]
        ], 1);

        self::assertCount(1, $slots);
        self::assertIsArray( $slots);

        /** @var Slot $slot */
        $slot = array_pop($slots);

        self::assertInstanceOf(Slot::class, $slot);
        self::assertEquals(true, $slot->getAvailable());
        self::assertInstanceOf(Currency::class, $slot->getCurrency());
        self::assertInstanceOf(Pitch::class, $slot->getPitch());
        self::assertEquals(10.51, $slot->getPrice());
    }
}
