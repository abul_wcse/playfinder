<?php

namespace PlayFinder\Tests\UnitTest\Exception;

use PHPUnit\Framework\TestCase;
use PlayFinder\Exception\ContentNotFoundException;

class ContentNotFoundExceptionTest extends TestCase
{
    public function testErrorCode(): void
    {
        $exception = new ContentNotFoundException();
        self::assertEquals(404, $exception->getCode());
    }

    public function testErrorMessage(): void
    {
        $errorMsg = 'Error Occurred';
        $exception = new ContentNotFoundException($errorMsg);
        self::assertEquals($errorMsg, $exception->getMessage());
    }
}
