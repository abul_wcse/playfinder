<?php

namespace PlayFinder\Tests\UnitTest\Exception;

use PHPUnit\Framework\TestCase;
use PlayFinder\Exception\InvalidDataException;

class InvalidDataExceptionTest extends TestCase
{
    public function testErrorCode(): void
    {
        $exception = new InvalidDataException();
        self::assertEquals(400, $exception->getCode());
    }

    public function testErrorMessage(): void
    {
        $errorMsg = 'Error Occurred';
        $exception = new InvalidDataException($errorMsg);
        self::assertEquals($errorMsg, $exception->getMessage());
    }
}
