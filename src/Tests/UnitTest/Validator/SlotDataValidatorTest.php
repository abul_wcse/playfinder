<?php

namespace PlayFinder\Tests\UnitTest\Validator;

use DateTime;
use PHPUnit\Framework\TestCase;
use PlayFinder\Entity\Currency;
use PlayFinder\Exception\InvalidDataException;
use PlayFinder\Repository\CurrencyRepository;
use PlayFinder\Validator\SlotDataValidator;

class SlotDataValidatorTest extends TestCase
{

    private $currencyRepository;

    public function setUp(): void
    {
        $this->currencyRepository = self::getMockBuilder(CurrencyRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testValidateWithNonArray(): void
    {
        $validator = new SlotDataValidator($this->currencyRepository);
        $this->expectExceptionObject(new InvalidDataException("Type Error: data should have array of slots"));

        $validator->validate('hello');
    }

    public function testValidateCurrencyFailure(): void
    {
        $this->currencyRepository->expects($this->once())->method('findOneBy')->willReturn(null);
        $validator = new SlotDataValidator($this->currencyRepository);
        $this->expectExceptionObject(new InvalidDataException('Currency $ is not supported'));

        $validator->validate([
            [
                'type' => 'slots',
                'id' => null,
                'attributes' => [
                    'starts' => (new DateTime())->format('c'),
                    'ends' => (new DateTime())->format('c'),
                    'currency' => '$',
                    'price' => 10.51,
                    'available' => true,
                ]
            ]
        ]);
    }

    public function testValidateSuccess(): void
    {
        $this->currencyRepository->expects($this->once())->method('findOneBy')->willReturn(new Currency());
        $validator = new SlotDataValidator($this->currencyRepository);
        $result = $validator->validate([
            [
                'type' => 'slots',
                'id' => null,
                'attributes' => [
                    'starts' => (new DateTime())->format('c'),
                    'ends' => (new DateTime())->format('c'),
                    'currency' => '$',
                    'price' => 10.51,
                    'available' => true,
                ]
            ]
        ]);

        self::assertTrue($result);
    }
}
