<?php

namespace PlayFinder\Factory;

use DateTime;
use PlayFinder\Entity\Slot;
use PlayFinder\Repository\SlotRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Slot|Proxy createOne(array $attributes = [])
 * @method static Slot[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static Slot|Proxy findOrCreate(array $attributes)
 * @method static Slot|Proxy random(array $attributes = [])
 * @method static Slot|Proxy randomOrCreate(array $attributes = [])
 * @method static Slot[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Slot[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static SlotRepository|RepositoryProxy repository()
 * @method Slot|Proxy create($attributes = [])
 */
final class SlotFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://github.com/zenstruck/foundry#factories-as-services)
    }

    protected function getDefaults(): array
    {
        $startTime = self::faker()->dateTimeBetween('now', '+1year');
        $endTime = new DateTime();
        $endTime->setTimestamp(($startTime->getTimestamp() + self::faker()->randomElement([30, 60, 120, 280, 240, 480])));
        return [
            'start_time' => $startTime,
            'end_time' => $endTime,
            'price' => self::faker()->randomFloat(2, 10, 400),
            'available' => self::faker()->boolean()
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this
            // ->afterInstantiate(function(Slot $slot) {})
        ;
    }

    protected static function getClass(): string
    {
        return Slot::class;
    }
}
