<?php

namespace PlayFinder\Factory;

use NumberFormatter;
use PlayFinder\Entity\Currency;
use PlayFinder\Repository\CurrencyRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Currency|Proxy createOne(array $attributes = [])
 * @method static Currency[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static Currency|Proxy findOrCreate(array $attributes)
 * @method static Currency|Proxy random(array $attributes = [])
 * @method static Currency|Proxy randomOrCreate(array $attributes = [])
 * @method static Currency[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Currency[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static CurrencyRepository|RepositoryProxy repository()
 * @method Currency|Proxy create($attributes = [])
 */
final class CurrencyFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://github.com/zenstruck/foundry#factories-as-services)
    }

    protected function getDefaults(): array
    {

        $fmt = new NumberFormatter( self::faker()->locale , NumberFormatter::CURRENCY );

        return [
            'symbol' => $fmt->getSymbol(NumberFormatter::CURRENCY_SYMBOL),
            'name' => $fmt->getTextAttribute(NumberFormatter::CURRENCY_CODE)
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this
            // ->afterInstantiate(function(Currency $currency) {})
        ;
    }

    protected static function getClass(): string
    {
        return Currency::class;
    }
}
