<?php

namespace PlayFinder\Factory;

use Faker\Provider\Address;
use PlayFinder\Entity\Pitch;
use PlayFinder\Repository\PitchRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Pitch|Proxy createOne(array $attributes = [])
 * @method static Pitch[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static Pitch|Proxy findOrCreate(array $attributes)
 * @method static Pitch|Proxy random(array $attributes = [])
 * @method static Pitch|Proxy randomOrCreate(array $attributes = [])
 * @method static Pitch[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Pitch[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static PitchRepository|RepositoryProxy repository()
 * @method Pitch|Proxy create($attributes = [])
 */
final class PitchFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://github.com/zenstruck/foundry#factories-as-services)
    }

    protected function getDefaults(): array
    {
        $sport = self::faker()->randomElement(['cricket', 'badminton', 'tennis', 'football', 'basketball', 'gym', 'squash']);
        return [
            'name' => self::faker()->streetName . ' ' . ucwords($sport) . ' Club',
            'sport' => $sport
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this
            // ->afterInstantiate(function(Pitch $pitch) {})
        ;
    }

    protected static function getClass(): string
    {
        return Pitch::class;
    }
}
